import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimeCalendarPage } from './time-calendar.page';

const routes: Routes = [
  {
    path: '',
    component: TimeCalendarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimeCalendarPageRoutingModule {}
