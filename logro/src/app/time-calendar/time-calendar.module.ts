import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimeCalendarPageRoutingModule } from './time-calendar-routing.module';

import { TimeCalendarPage } from './time-calendar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TimeCalendarPageRoutingModule
  ],
  declarations: [TimeCalendarPage]
})
export class TimeCalendarPageModule {}
