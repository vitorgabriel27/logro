import { Component, OnInit } from '@angular/core';
import { LoginPage } from '../login/login.page';
import { SignUpPage } from '../sign-up/sign-up.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-init',
  templateUrl: './init.page.html',
  styleUrls: ['./init.page.scss'],
})
export class InitPage implements OnInit {

  constructor( public modalController:ModalController ) { }

  ngOnInit() {
  }

  async openLogin() {
    const modal = await this.modalController.create({
      component: LoginPage,
      initialBreakpoint: 0.85,
      breakpoints: [0,0.85],
      cssClass: 'loginModal'
    });
    return await modal.present();
  }

  async openSignUp() {
    const modal = await this.modalController.create({
      component: SignUpPage,
      initialBreakpoint: 0.85,
      breakpoints: [0,0.9],
      cssClass: 'signUpModal'
    });
    return await modal.present();
  }
}
