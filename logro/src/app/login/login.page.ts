import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SignUpPage } from '../sign-up/sign-up.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor( public modalController:ModalController) { }

  ngOnInit() {
  }

  user: any = {email: '', password: ''}

  async openSignUp() {
    const modal = await this.modalController.create({
      component: SignUpPage,
      initialBreakpoint: 0.85,
      breakpoints: [0,0.9],
      cssClass: 'signUpModal'
    });
    return await modal.present();
  }

  dismiss() {
  
    this.modalController.dismiss({
      'dismissed': true
    });
  }

}
