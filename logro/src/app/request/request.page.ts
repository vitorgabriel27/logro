import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PaymentPage } from '../payment/payment.page';

@Component({
  selector: 'app-request',
  templateUrl: './request.page.html',
  styleUrls: ['./request.page.scss'],
})
export class RequestPage implements OnInit {

  constructor( public modalController:ModalController ) { }

  ngOnInit() {
  }

  public properties = [
    {
      name: "Período",
      propertie: "De 15/09 a 20/09 - Tarde",
    },
    {
      name: "Nome do local",
      propertie: "Condomínio Ouro Branco",
    },
    
    {
      name: "Tipo de obra",
      propertie: "Condomínio",
    },
    {
      name: "Profissional",
      propertie: "Arquiteto(a)",
    },
    {
      name: "Construtora",
      propertie: "Construtora Doce Lar",
    },
    {
      name: "Responsável técnico:",
      propertie: "Lamar Rodouph",
    },
    {
      name: "Telefone:",
      propertie: "(84) 9 9999 9999",
    },
    {
      name:"Tipo de inspeção",
      propertie: "Individual",
    }
  ];

  async openPayment() {
    const modal = await this.modalController.create({
      component:PaymentPage,
      initialBreakpoint: 0.5,
      breakpoints: [0,0.5],
      cssClass: 'paymentModal'
    });
    return await modal.present();
  }

 
}
