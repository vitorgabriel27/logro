import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Inicio', url: '/home', icon: 'user' },
    { title: 'Minhas Inspeções', url: '/my-inspections', icon: 'square' },
    { title: 'Carteira', url: '/wallet', icon: 'usd' },

    
  ];

  public persons = [
    {
      name: 'Maria José',
      rate: 4.90,
    },
  
  ];
  
  constructor( private menu: MenuController) {}

  closeMenu() {
    this.menu.close('menu');
  }
}
