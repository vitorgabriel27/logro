import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(private menu: MenuController) { }

  ngOnInit() {
  }

  public notifications = [
    {
      title: 'Sua solicitação foi aceita.',
    },
    {
      title: 'Sua inspeção foi concluida. Confira',
    },
    {
      title: 'Confirme sua solicitação.',
    },
  ];

  public noteLength = this.notifications.length

  openMenu() {
    this.menu.open('menu');
  }

  openNotify() {
    this.menu.open('notify');
  }

}
