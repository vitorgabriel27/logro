import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  constructor( public modalController:ModalController ) { }

  ngOnInit() {
  }

  public payments = [
    {
      title: 'Deslocamento',
      price:'00,00',
    },
    {
      title: 'Hospedagem',
      price:'00,00',
    },
    {
      title: 'Alimentação',
      price:'00,00',
    },
  ];

  dismiss() {
  
    this.modalController.dismiss({
      'dismissed': true
    });
  }

}
