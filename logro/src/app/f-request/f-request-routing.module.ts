import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FRequestPage } from './f-request.page';

const routes: Routes = [
  {
    path: '',
    component: FRequestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FRequestPageRoutingModule {}
