import { Component, OnInit } from '@angular/core';
import * as Leaflet from 'leaflet';
import { antPath } from 'leaflet-ant-path';



@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

  map: Leaflet.Map;

  latlngs = [
    [-5.8109583,-35.2182338],
    [-5.815415, -35.206115],
    [-5.816591, -35.206563],
    [-5.8275046,-35.2109531],
    [-5.832427, -35.212176],
    
  ];

  options = { use: Leaflet.polyline, delay: 400, dashArray: [10,20], weight: 5, color: "#FFF", pulseColor: "#E94E1B" };

  path = antPath(this.latlngs, this.options);

  gIcon = Leaflet.icon({
    iconUrl: '/assets/logro.svg',

    iconSize:     [38, 95], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
  });

  locationIcon = Leaflet.icon({
    iconUrl: '/assets/icon/location-dot.svg',

    iconSize:     [28, 85], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

  constructor() { }


  ngOnInit() {

    var Url = 'https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png';
	  var Attrib = '&copy;<a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors ';
	  

  
    this.map = Leaflet.map('mapId',{ minZoom:10, zoomControl: false}).setView([-5.8109583,-35.2182338], 16  );
    Leaflet.tileLayer(Url, {attribution: Attrib,}).addTo(this.map);

    Leaflet.marker([-5.8109583,-35.2182338], {icon: this.locationIcon}).addTo(this.map);
    Leaflet.marker([-5.832427, -35.21215], {icon: this.gIcon}).addTo(this.map);

    this.map.addLayer(this.path);
    this.map.fitBounds(this.path.getBounds())
     
    
  }

  ngAfterContentChecked(){
    setTimeout(() => {
      this.map.invalidateSize()
    }, 0);
    
  };
    
  
    

 

  /** Remove map when we have multiple map object */
  ngOnDestroy() {
   
    this.map.remove();
  }


}
