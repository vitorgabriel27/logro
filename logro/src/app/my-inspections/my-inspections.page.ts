import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-inspections',
  templateUrl: './my-inspections.page.html',
  styleUrls: ['./my-inspections.page.scss'],
})
export class MyInspectionsPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public inspections = [
    {
      name: 'Nome da Solicitação',
      status: 'Finalizada',
    },
    {
      name: 'Nome da Solicitação',
      status: 'Finalizada',
    },
    {
      name: 'Nome da Solicitação',
      status: 'Finalizada',
    },
    {
      name: 'Nome da Solicitação',
      status: 'Finalizada',
    },
    {
      name: 'Nome da Solicitação',
      status: 'Finalizada',
    },
    {
      name: 'Nome da Solicitação',
      status: 'Finalizada',
    },
    {
      name: 'Nome da Solicitação',
      status: 'Finalizada',
    },
    {
      name: 'Nome da Solicitação',
      status: 'Finalizada',
    },
 
    
  ];
}
