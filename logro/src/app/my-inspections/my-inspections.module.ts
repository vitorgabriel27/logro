import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyInspectionsPageRoutingModule } from './my-inspections-routing.module';

import { MyInspectionsPage } from './my-inspections.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyInspectionsPageRoutingModule
  ],
  declarations: [MyInspectionsPage]
})
export class MyInspectionsPageModule {}
