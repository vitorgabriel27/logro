import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public slideOpts: any = {
    speed: 400,
    slidesPerView: 3,
    freeMode: true,
  };

  public images = [
    {
      title: 'Titulo',
      imgURl:'https://picsum.photos/500',
    },
    {
      title: 'Titulo',
      imgURl:'https://picsum.photos/500',
    },
    {
      title: 'Titulo',
      imgURl:'https://picsum.photos/500',
    },
    {
      title: 'Titulo',
      imgURl:'https://picsum.photos/500',
    },
    {
      title: 'Titulo',
      imgURl:'https://picsum.photos/500',
    }

  ];

  public properties = [
    {
      name: "Período",
      propertie: "De 15/09 a 20/09 - Tarde",
    },
    {
      name: "Nome do local",
      propertie: "Condomínio Ouro Branco",
    },
    
    {
      name: "Tipo de obra",
      propertie: "Condomínio",
    },
    {
      name: "Profissional",
      propertie: "Arquiteto(a)",
    },
    {
      name: "Construtora",
      propertie: "Construtora Doce Lar",
    },
    {
      name: "Responsável técnico:",
      propertie: "Lamar Rodouph",
    },
    {
      name: "Telefone:",
      propertie: "(84) 9 9999 9999",
    },
    {
      name:"Tipo de inspeção",
      propertie: "Individual",
    }
  ];

  public files = [
    {
      title: "Nome do arquivo",
    },
    {
      title: "Nome do arquivo",
    },
    {
      title: "Nome do arquivo",
    },
    {
      title: "Nome do arquivo",
    },


  
  ];
}
