import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewCardPage } from './new-card.page';
import { CardStep1Component } from './components/card-step1/card-step1.component';
import { CardStep2Component } from './components/card-step2/card-step2.component';
import { CardStep3Component } from './components/card-step3/card-step3.component';
import { CardStep4Component } from './components/card-step4/card-step4.component';
import { CardStep5Component } from './components/card-step5/card-step5.component';



const routes: Routes = [
  { path: '', component: NewCardPage, },  
  { path: '', redirectTo: '/new-card', pathMatch: 'full' },
    
 
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
exports: [RouterModule],
})
export class NewCardPageRoutingModule {}
