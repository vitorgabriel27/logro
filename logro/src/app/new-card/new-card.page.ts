import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-card',
  templateUrl: './new-card.page.html',
  styleUrls: ['./new-card.page.scss'],
})
export class NewCardPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  card: any = {number: '',name: '',validate: '',secret: '',credit: false, debit: false};

  id = 1;

  cardAdd(num: string) {
    this.card.number = num;
  }
  cardAddName(name: string) {
    this.card.name = name;
  }
  cardAddValidate(validate: string) {
    this.card.validate = validate;
  }
  cardAddSecret(secret: string) {
    this.card.secret = secret;
  }
  cardAddType(val1: any) {
    this.card.credit = val1.credit;
    this.card.debit = val1.debit;
  }

  incrementId(val){
    this.id = val;
  }

  onCardStep(){
  
    this.id = 1;
  }

}