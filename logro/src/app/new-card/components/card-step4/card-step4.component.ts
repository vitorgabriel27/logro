import { Component, OnInit, Output, Input, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-card-step4',
  templateUrl: './card-step4.component.html',
  styleUrls: ['./card-step4.component.scss'],
})
export class CardStep4Component implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  @Input() card: any = {number: '',name: '',validate: '',secret: '',credit: false, debit: false};

  secret:string = '';

  @Output() cardEvent = new EventEmitter<string>();

  cardChange(value: string) {
    this.cardEvent.emit(value);
  }
  constructor() { }

  ngOnInit() {}

  public data = [
    {
      id: 4,      
      number: '**** **** **** 1314',
      validate: '12/31',
      name: 'Maria José',
      secret: '',

    },
  ];
}
