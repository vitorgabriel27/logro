import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card-step2',
  templateUrl: './card-step2.component.html',
  styleUrls: ['./card-step2.component.scss'],
})
export class CardStep2Component implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

 
  constructor() { }

  ngOnInit() {}

  @Input() card: any = {number: '',name: '',validate: '',secret: '',credit: false, debit: false};

  name:string = '';

  @Output() cardEvent = new EventEmitter<string>();

  cardChange(value: string) {
    this.cardEvent.emit(value);
  }


}
