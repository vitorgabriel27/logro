import { Component, OnInit, Input, Output,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

  @Input() id: number;

  @Output() idEvent = new EventEmitter<number>();

  onNextStep(){
 
    if(this.id !== 5){
      this.id++;
      this.idEvent.emit(this.id);
    }
  }

  onPreviousStep(){
    if(this.id !== 1){
      this.id--;
      this.idEvent.emit(this.id);
    }
  }

  resetId(){
    this.id = 1;
    this.idEvent.emit(this.id);
  }
}
