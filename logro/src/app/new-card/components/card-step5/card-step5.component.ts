import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card-step5',
  templateUrl: './card-step5.component.html',
  styleUrls: ['./card-step5.component.scss'],
})
export class CardStep5Component implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  @Input() card: any = {number: '',name: '',validate: '',secret: '',credit: false, debit: false};

  type: any = {
    credit: false,
    debit: false
  }
 

  @Output() cardEvent = new EventEmitter<any>();

  cardChange(type: any) {
    this.cardEvent.emit(type);
  }

  onCredit(){
    this.type.credit = !this.type.credit
  }
  onDebit(){
    this.type.debit = !this.type.debit
  }

  constructor() { }

  ngOnInit() {}

 
}
