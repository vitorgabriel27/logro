import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-card-step1',
  templateUrl: './card-step1.component.html',
  styleUrls: ['./card-step1.component.scss'],
})
export class CardStep1Component implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  card: any = {number: ''};

  @Output() cardEvent = new EventEmitter<string>();

  cardChange(value: string) {
    this.cardEvent.emit(value);
  }

  constructor() { }

  ngOnInit() {
    
  }

  

}
