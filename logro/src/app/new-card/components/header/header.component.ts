import { Component, OnInit, Output, EventEmitter,Input} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

  @Input() id: number;

  @Output() idEvent = new EventEmitter<number>();

  resetId(){
    this.id = 1;
    this.idEvent.emit(this.id);
  }

}
