import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card-step3',
  templateUrl: './card-step3.component.html',
  styleUrls: ['./card-step3.component.scss'],
})
export class CardStep3Component implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  @Input() card: any = {number: '',name: '',validate: '',secret: '',credit: false, debit: false};

  validate:string;
  
  @Output() cardEvent = new EventEmitter<string>();

  cardChange(value: string) {
    this.cardEvent.emit(value);
  }



  constructor() { }

  ngOnInit() {}



}
