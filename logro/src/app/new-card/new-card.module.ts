import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewCardPageRoutingModule } from './new-card-routing.module';
import { NewCardPage } from './new-card.page';

import { CardStep1Component } from './components/card-step1/card-step1.component';
import { CardStep2Component } from './components/card-step2/card-step2.component';
import { CardStep3Component } from './components/card-step3/card-step3.component';
import { CardStep4Component } from './components/card-step4/card-step4.component';
import { CardStep5Component } from './components/card-step5/card-step5.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { NgxMaskModule } from 'ngx-mask';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewCardPageRoutingModule,
    NgxMaskModule.forChild()
    
  ],
  declarations: [
    NewCardPage,
    CardStep1Component,
    CardStep2Component,
    CardStep3Component,
    CardStep4Component,
    CardStep5Component,
    HeaderComponent,
    FooterComponent,
  ]
})
export class NewCardPageModule {}
