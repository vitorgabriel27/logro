import { Component, OnInit } from '@angular/core';
import { LoginPage } from '../login/login.page';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {

  constructor( public modalController:ModalController ) { }

  ngOnInit() {
  }

  user: any = {email: '', password: ''}

  async openLogin() {
    const modal = await this.modalController.create({
      component: LoginPage,
      initialBreakpoint: 0.85,
      breakpoints: [0,0.85],
      cssClass: 'loginModal'
    });
    return await modal.present();
  }

  dismiss() {
  
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
