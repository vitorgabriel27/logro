import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { IonicModule } from '@ionic/angular';

import { EvaluatePageRoutingModule } from './evaluate-routing.module';

import { EvaluatePage } from './evaluate.page';
import { NgxStarRatingModule } from 'ngx-star-rating';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EvaluatePageRoutingModule,
    NgxStarRatingModule,
    ReactiveFormsModule, 

  ],
  declarations: [EvaluatePage]
})
export class EvaluatePageModule {}
