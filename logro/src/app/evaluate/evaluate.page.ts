import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-evaluate',
  templateUrl: './evaluate.page.html',
  styleUrls: ['./evaluate.page.scss'],
})
export class EvaluatePage implements OnInit {

  

  rating1: number;

  constructor(){
   
    this.rating1 = 0;
  }

  ngOnInit() {
  }

  public rate = [
    {
      id: 1,
      title: 'Péssimo',
    },
    {
      id: 2,
      title: 'Ruim',
    },
    {
      id: 3,
      title: 'Bom',
    },
    {
      id: 4,
      title: 'Muito Bom',
    },
    {
      id: 5,
      title: 'Excelente',
    },
  ];

}
