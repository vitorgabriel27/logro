import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-inspection',
  templateUrl: './new-inspection.page.html',
  styleUrls: ['./new-inspection.page.scss'],
})
export class NewInspectionPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public isCollapsed = true;
  public isCollapsed1 = true;

  inspection: any = {
    type: '',
    location: '',
    service: '',
    inCharge: '',
    company: '',

  }

  public options = [
    { 
      option: 'Inspeção Individual',
    },
    { 
      option: 'Análise de Riscos',
     
    },
    { 
      option: '',
    },
  ];

  public profissional = [
    { 
      option: 'Engenheiro (a)',
    },
    { 
      option: 'Arquiteto (a)',
     
    },
    { 
      option: 'Técnico (a)',
    },
  ];

  public datec = [
    {
      num: 'DATEC nº 001-A' 
    },
    {
      num: 'DATEC nº 002' 
    },
    {
      num: 'DATEC nº 003-B' 
    },
    {
      num: 'DATEC nº 004' 
    },
    {
      num: 'DATEC nº 005-C' 
    },
    {
      num: 'DATEC nº 006-A' 
    },
    {
      num: 'DATEC nº 007-A' 
    },
    {
      num: 'DATEC nº 008-A' 
    },
    {
      num: 'DATEC nº 009-A' 
    },

  
  ];

  public innovation = [
    {
      name: 'Dry Wall',
    },
    {
      name: 'Pré-moldados',
    },
    {
      name: 'Paredes de Concreto',
    },
    {
      name: 'Light Steel Frame',
    },
    {
      name: 'Wood Frame',
    },
    {
      name: 'Containers',
    },
    {
      name: 'Impressão 3D',
    },
  ];
}
